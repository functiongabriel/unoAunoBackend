## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```
# Variables de Entorno:

- `DATABASE_URL`: Connexion mongoDB
- `JWT_SECRET`: palabra secreta para firmar token

# Comandos para actualizar MongoDB y prisma:

1. `npx prisma generate`: Actualizar schema del proyecto

## Diagram DB->Colecciones de la Base de Datos Mongodb

### Colección: Traveler

| Campo         | Tipo de Dato |
| ------------- | ------------ |
| _id           | ObjectID     |
| date_of_birth | Date         |
| document      | String       |
| name          | String       |
| phone         | String       |
| trips         | String<Trip._id>[]  

### Colección: Trip

| Campo       | Tipo de Dato |
| ----------- | ------------ |
| _id         | ObjectID     |
| code_number | String       |
| seats       | Number       |
| to          | String       |
| from        | String       |
| price       | Number       |

## Swagger URL 
http://localhost:3000/api#/
OR
http://localhost:3333/api#/

## Mis variables de entornos temporales para la conexion a la DB Remota con propositos de la prueba

DATABASE_URL="mongodb+srv://root:171299gabo@projects.y1wyk5w.mongodb.net/unoAuno?retryWrites=true&w=majority"
